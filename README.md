# ~/ABRÜPT/MICROANTILIVRES/*

Le [microantilivre](https://abrupt.ch/antilivre#microantilivre) : Il s’agit de la version clandestine de l’antilivre, sa version brute et punk, celle qui hurle DIY et s’imprime à la va-vite, à la volée des bureaux et des ennuis, en une page, son recto puis son verso, deux pliages, deux agrafes, un coup de ciseaux, et voilà le verbe qui se pirate, et voilà l’antilivre qui se propose, la microscopie de son évasion qui porte le rêve, irise le quotidien d’un peu d’obscur.

La [liste](https://abrupt.ch/cyberpoetique/tags/microantilivre/) de nos microantilivres est publiée dans notre [Cyberpoétique](https://abrupt.ch/cyberpoetique/).


## Dans le domaine public volontaire

Tous les microantilivres sont dédiés au domaine public.

Ils sont mis à disposition selon les termes de la Licence Creative Commons Zero (CC0 1.0 Universel).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.ch) pour davantage d’informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.ch/partage/).

